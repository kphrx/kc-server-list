// Tagged template literal: https://developer.mozilla.org/docs/Web/JavaScript/Reference/template_strings#Tagged_templates
const pathTmpl = (strings, ...keys) => {
  return (...values) => {
    const dict = values[values.length - 1] || {};
    const result = [strings[0]];
    keys.forEach((key, i) => {
      const value = Number.isInteger(key) ? values[key] : dict[key];
      result.push(value, strings[i + 1]);
    });
    return result.join("");
  };
};

/*
 * interface ConstServerInfo {
 *   Gadget?: string;
 *   World_1: string;
 *   World_n: string;
 *   OSAPI?: string;
 *   NETGAME?: string;
 * }
 * renderServerList(dl: HTMLElement, serverInfo: ConstServerInfo, pathTemplate: (values: { host?: string, world?: string, name?: string }) => string) (dl: HTMLElement)
 */
const renderServerList = (dl, serverInfo, pathTemplate) => {
  if (dl == null || !(dl instanceof HTMLElement)) {
    dl = document.createElement("dl");
  }
  if (serverInfo == null || typeof serverInfo !== "object") {
    serverInfo = {};
  }
  if (pathTemplate == null || typeof pathTemplate !== "function") {
    pathTemplate =
      pathTmpl`http://${"host"}/kcs2/resources/world/${"name"}.png`;
  }

  for (name of Object.keys(serverInfo)) {
    if (!name.startsWith("World_")) continue;

    const url = new URL(serverInfo[name]);

    const server = dl.appendChild(document.createElement("div"));
    server.id = `${name}`;
    server.classList.add("server");

    const ip = server.appendChild(document.createElement("dt"));
    ip.classList.add("address");
    ip.appendChild(document.createTextNode(url.hostname));

    const ipString = url.hostname.split(".").map((x) => `000${x}`.slice(-3))
      .join(
        "_",
      );

    const serverName = server.appendChild(document.createElement("dd"));
    serverName.classList.add("name");

    const pathConstruct = { host: url.hostname, world: name.toLowerCase() };
    const nameTitleImage = document.createElement("span");
    nameTitleImage.classList.add("image");
    const nameSmallImage = nameTitleImage.cloneNode();

    nameTitleImage.appendChild(new Image()).src = pathTemplate(
      Object.assign({ name: `${ipString}_t` }, pathConstruct),
    );
    nameTitleImage.classList.add("title");

    nameSmallImage.appendChild(new Image()).src = pathTemplate(
      Object.assign({ name: `${ipString}_s` }, pathConstruct),
    );
    nameSmallImage.classList.add("small");

    serverName.append(nameTitleImage, nameSmallImage);
  }

  return dl;
};

/*
 * interface VersionInfo {
 *   scriptVesion: string;
 * }
 * renderVersion(span: HTMLElement, versionInfo: VersionInfo) (span: HTMLElement)
 */
const renderVersion = (span, versionInfo) => {
  if (span == null || !(span instanceof HTMLElement)) {
    span = document.createElement("span");
  }
  if (
    versionInfo == null || typeof versionInfo !== "object" ||
    versionInfo.scriptVesion == null
  ) {
    versionInfo = { scriptVesion: "4.5.9.0" };
  }

  span.appendChild(document.createTextNode(versionInfo.scriptVesion));

  return span;
};

/*
 * interface MaintenanceInfo {
 *   IsDoing: number;
 *   IsEmergency: number;
 *   StartDateTime: Date | number;
 *   EndDateTime: Date | number;
 * }
 * renderUpdated(span: HTMLElement, maintenanceInfo: MaintenanceInfo) (span: HTMLElement)
 */
const renderUpdated = (span, maintenanceInfo) => {
  if (span == null || !(span instanceof HTMLElement)) {
    span = document.createElement("span");
  }
  if (
    maintenanceInfo == null || typeof maintenanceInfo !== "object" ||
    maintenanceInfo.EndDateTime == null
  ) {
    maintenanceInfo = {
      IsDoing: 0,
      IsEmergency: 0,
      StartDateTime: Date.parse("2020/06/19 14:00:00"),
      EndDateTime: Date.parse("2020/06/19 18:00:00"),
    };
  }

  if (maintenanceInfo.IsDoing || maintenanceInfo.IsEmergency) {
    span.innerHTML = "Maintenance:&nbsp;" +
      (new Date(maintenanceInfo.StartDateTime)).toLocaleString("ja").replace(
        " ",
        "&nbsp;",
      ) +
      "&nbsp;~&nbsp;" +
      (new Date(maintenanceInfo.EndDateTime)).toLocaleString("ja").replace(
        " ",
        "&nbsp;",
      );
  } else {
    span.appendChild(
      document.createTextNode(
        (new Date(maintenanceInfo.EndDateTime)).toLocaleString("ja").replace(
          " ",
          "\u00A0",
        ),
      ),
    );
  }

  return span;
};

export { pathTmpl, renderServerList, renderUpdated, renderVersion };
