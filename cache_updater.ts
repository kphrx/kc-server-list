import { toLines } from "@std/streams/unstable-to-lines";
import { exists } from "@std/fs/exists";
import { join } from "@std/path/join";
import { encodeHex } from "@std/encoding/hex";
import { crypto } from "@std/crypto/crypto";

export const CACHE_DIR = "kc_const_cache";
const CACHE_DIGEST = join(CACHE_DIR, "kcs_const.js.sha256");
const CACHE_VERSION = join(CACHE_DIR, "version.txt");
export const CACHE_SERVERS = join(CACHE_DIR, "worlds.json");
const CACHE_MAINTENANCE = join(CACHE_DIR, "maintenance.json");

function fetchKcsConst(): Promise<ReadableStream<Uint8Array>> {
  return fetch(
    "http://w00g.kancolle-server.com/gadget_html5/js/kcs_const.js",
  ).then((res) => {
    const { body, status, statusText } = res;
    if (status !== 200 || body == null) {
      throw statusText;
    }

    return body;
  });
}

async function sha256sum(body: ReadableStream<Uint8Array>): Promise<string> {
  const digest = await crypto.subtle.digest("SHA-256", body);

  return encodeHex(digest);
}

async function currentHash(): Promise<string | null> {
  if (await exists(CACHE_DIGEST)) {
    return Deno.readTextFile(CACHE_DIGEST);
  }

  return null;
}

async function checkCachedVersion(body: ReadableStream<Uint8Array>) {
  const newHash = await sha256sum(body);

  return {
    isUpToDate: await currentHash() === newHash,
    postUpdate: async () => {
      await Deno.writeTextFile(CACHE_DIGEST, newHash);
    },
  };
}

interface KcsConst {
  versionInfo: string;
  serverInfo: Record<string, string>;
  maintenanceInfo: {
    IsDoing: number;
    IsEmergency: number;
    StartDateTime: number;
    EndDateTime: number;
  };
}

export async function parseKcsConst(
  kcsConst: ReadableStream<Uint8Array>,
): Promise<KcsConst> {
  let versionInfo: KcsConst["versionInfo"] | null = null;
  const serverInfo: KcsConst["serverInfo"] = {},
    maintenanceInfo: KcsConst["maintenanceInfo"] = {
      IsDoing: 0,
      IsEmergency: 0,
      StartDateTime: 0,
      EndDateTime: 0,
    };

  for await (const line of toLines(kcsConst)) {
    const versionInfoMatched: RegExpMatchArray | null = versionInfo == null
      ? line.match(/^VersionInfo.scriptVesion\s*?=\s*?"(.+)";/)
      : null;
    if (versionInfoMatched != null) {
      versionInfo = versionInfoMatched[1];

      continue;
    }

    const maintenanceInfoMatched: RegExpMatchArray | null =
      maintenanceInfo.EndDateTime === 0
        ? line.match(
          /^MaintenanceInfo.(IsDoing|IsEmergency|StartDateTime|EndDateTime)\s*?=\s*?([^\s].*);/,
        )
        : null;
    if (maintenanceInfoMatched != null) {
      const [name, value] = maintenanceInfoMatched.slice(1) as [
        keyof KcsConst["maintenanceInfo"],
        string,
      ];
      switch (name) {
        case "IsDoing":
        case "IsEmergency":
          maintenanceInfo[name] = parseInt(value);
          break;
        case "StartDateTime":
        case "EndDateTime":
          maintenanceInfo[name] = Date.parse(
            (value.match(/Date\.parse\("(.+)"\)/)?.[1] ?? "0") + "+9",
          );
          break;
      }

      continue;
    }

    const serverInfoMatched: RegExpMatchArray | null = line.match(
      /^ConstServerInfo.(World_[0-9]*)\s*?=\s*?"(.+)";/,
    );
    if (serverInfoMatched != null) {
      const [name, address] = serverInfoMatched.slice(1);
      serverInfo[name] = address;

      continue;
    }
  }

  if (versionInfo == null) {
    throw "Not found VersionInfo";
  }

  return {
    versionInfo,
    serverInfo,
    maintenanceInfo,
  };
}

async function cacheVersion(): Promise<string | null> {
  if (await exists(CACHE_VERSION)) {
    return Deno.readTextFile(CACHE_VERSION);
  }

  return null;
}

async function updateCache(
  { versionInfo, serverInfo, maintenanceInfo }: KcsConst,
) {
  const cachedVersion = await cacheVersion();
  cachedVersion != null && console.log(`cached version: "%s"`, cachedVersion);
  console.log(`new version: "%s"`, versionInfo);

  await Deno.mkdir(CACHE_DIR, { recursive: true });
  await Deno.writeTextFile(CACHE_VERSION, versionInfo);
  await Deno.writeTextFile(
    CACHE_SERVERS,
    JSON.stringify(serverInfo, undefined, 2) + "\n",
  );
  await Deno.writeTextFile(
    CACHE_MAINTENANCE,
    JSON.stringify(maintenanceInfo, undefined, 2) + "\n",
  );

  console.log("updated");
}

// Learn more at https://docs.deno.com/runtime/manual/examples/module_metadata#concepts
if (import.meta.main) {
  try {
    const [tee1, tee2] = await fetchKcsConst().then((body) => body.tee());
    const { isUpToDate, postUpdate } = await checkCachedVersion(tee1);
    if (isUpToDate) {
      throw "already updated";
    }

    await updateCache(await parseKcsConst(tee2));
    await postUpdate();
  } catch (e) {
    console.error(e);
    console.log("no update");
  }
}
